#!/system/bin/sh

#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This script will run as a postinstall step to drive otapreopt.

TARGET_SLOT="$1"
STATUS_FD="$2"

# Maximum number of packages/steps.
MAXIMUM_PACKAGES=1000

# Reporting time interval.
REPORT_INTERVAL=5

# First ensure the system is booted. This is to work around issues when cmd would
# infinitely loop trying to get a service manager (which will never come up in that
# mode). b/30797145
BOOT_PROPERTY_NAME="dev.bootcomplete"

BOOT_COMPLETE=$(getprop $BOOT_PROPERTY_NAME)
if [ "$BOOT_COMPLETE" != "1" ] ; then
  echo "Error: boot-complete not detected."
  # We must return 0 to not block sideload.
  exit 0
fi


# Compute target slot suffix.
# TODO: Once bootctl is not restricted, we should query from there. Or get this from
#       update_engine as a parameter.
if [ "$TARGET_SLOT" = "0" ] ; then
  TARGET_SLOT_SUFFIX="_a"
elif [ "$TARGET_SLOT" = "1" ] ; then
  TARGET_SLOT_SUFFIX="_b"
else
  echo "Unknown target slot $TARGET_SLOT"
  exit 1
fi

PREPARE=$(cmd otadexopt prepareHn)
# Note: Ignore preparation failures. Step and done will fail and exit this.
#       This is necessary to support suspends - the OTA service will keep
#       the state around for us.

TIME_LIMIT=$(cmd otadexopt getTimeLimit)

update_progress() {
  local progress_time="$1"
  local progress=$(echo "scale=2; $progress_time/$TIME_LIMIT*0.99" | bc)
  local formatted_progress=$(printf "%.2f" "$progress")
  print -u${STATUS_FD} "global_progress $formatted_progress"
}

print_progress() {
  i=0
  while ((i<$TIME_LIMIT)) ; do
    update_progress $i &
    sleep $REPORT_INTERVAL
    i=$((i+$REPORT_INTERVAL))
  done

  cmd otadexopt stop
  wait
}

print_progress &

i=0
while ((i<MAXIMUM_PACKAGES)) ; do
  DONE=$(cmd otadexopt done)
  if [ "$DONE" = "OTA complete." ] ; then
    break
  fi

  DEXOPT_PARAMS=$(cmd otadexopt next)

  DONE=$(cmd otadexopt done)
  if [ "$DONE" = "OTA complete." ] ; then
    break
  fi

  /system/bin/otapreopt_chroot $STATUS_FD $TARGET_SLOT_SUFFIX $DEXOPT_PARAMS >&- 2>&-

  sleep 1
  i=$((i+1))
done

DONE=$(cmd otadexopt done)
if [ "$DONE" = "OTA incomplete." ] ; then
  echo "Incomplete."
else
  echo "Complete or error."
fi

cmd otadexopt cleanup
wait
print -u${STATUS_FD} "global_progress 1.0"

exit 0
